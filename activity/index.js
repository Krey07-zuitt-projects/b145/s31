const express = require('express');

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// MOCK DATABASE
let users = [
	{
		email: "pabloEscobar@mail.com",
		username: "pabloEscobar",
		password: "patron",
		isAdmin: false
	},
	{
		email: "sonGoku@mail.com",
		username: "saiyanMan",
		password: "saiyanGod",
		isAdmin: true
	},
	{
		email: "luffy@mail.com",
		username: "pirateKing",
		password: "food",
		isAdmin: false
	}
];


let loggedUser;

// 1
app.get('/home', (req, res) => {
	res.send('Welcome to the homepage')
});



// 3

app.get('/users', (req, res) => {
	res.send(users)
});


// 5

app.delete('/delete-user', (req, res) => {
	console.log(req.body);

	let delUser = users.find((user) => {
		return user.username = req.body.username
	})

	if(delUser !== undefined) {
		let delUserIndex = users.splice((user) =>{
			return user.username === delUser.username
		});

		res.send(`${req.body.username} has been deleted`)
	}

})


app.listen(port, () => console.log(`Server is running at port ${port}`))